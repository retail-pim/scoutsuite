# Scout Suite

[[_TOC_]]

## Purpose
[Scout Suite](https://github.com/nccgroup/ScoutSuite) analyzes your cloud project(s) for common security issues and best practices. 

This project exists for two reasons:
* To execute scheduled pipelines scanning our projects on a regular basis
* Currently they do not offer their own Docker image and the build of the python app takes >20 minutes which is not suitable for CI. Thus we provide our own docker image here to speed up the scans (no need to do the python build every time) until an offical container is available.

## What does it achieve?

A pipeline that can be scheduled to execute a ScoutSuite scan of both a dev and a production GCP cluster's configuration for security issues.

It produces:
* a full HTML / JS Report as an artifact for deep diving on the findings
* a summary sent as a slack notification to the webhook of your choice

## Who can use it?
Anyone can, of course, utilise the Dockerfile.

The rest of the project is a essentially a pipeline that assumes:
* GCP as the provider of the cloud project(s) you want to scan
* This is a Gitlab pipeline - can be easily adapted by you (or me on request) for others such as Github Actions
* A dev project and a production project exist to be scanned

Feel free to copy, change modify, twist or warp for your own needs :D

## Pre-Requisites

### 1. GCP Project Service Account(s)

In order to give ScoutSuite sufficient permissions to read what it needs to in the project, create a service account in both the development and the production projects with the following roles:

* Viewer
* Security Reviewer
* Stackdriver Account Viewer

Afterwards create a key for each of the service accounts which you will need for the next step

### 2. PROJECT CI/CD SECRET VARIABLES

#### 2.1 Mandatory Variables

The jobs expect the following CICD Variables to be present in order to function:

`DEV_PROJECT_ID` = `<project id of your development GCP Project>`

`DEV_SCOUTSUITE_SA_CREDS` = `<json formatted service account key for the development GCP Project>`

`PROD_PROJECT_ID` = `<project id of your production GCP Project>`

`PROD_SCOUTSUITE_SA_CREDS` = `<json formatted service account key for the production GCP Project>`

`SEC_REPORT_WEBHOOK` = `<the webhook to fire the reports at>`

#### 2.2 Optional Variables

It is useful, especially when toying with the ejs template, to have an alternative channel / webhook to fire test messages at without spamming the intended channel. This webhook will be used for any pipeline that is NOT a sheduled pipeline.

`TEST_WEBHOOK` = `<an alternative webhook to fire messages at during tests>`
