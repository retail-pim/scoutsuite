FROM alpine:latest

# Install pre-reqs for Scoutsuite & Slack Notifications (curl,jq, npm)
RUN apk upgrade --no-cache && \
    apk add --no-cache python3 py-pip py3-virtualenv git make g++ gcc musl-dev libffi-dev openssl-dev python3-dev curl npm jq

# Change version to be installed
ENV VERSION="5.10.1"

# Require for Slack Message Templating
RUN npm install -g ejs

# Artificially set a virtualenv to work in a docker context
ENV VIRTUAL_ENV=/ScoutSuite
RUN python3 -m virtualenv --python=/usr/bin/python3 ${VIRTUAL_ENV}
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

WORKDIR ${VIRTUAL_ENV}

# Install 
RUN pip install "scoutsuite==${VERSION}"

# Add custom ruleset
ADD custom-rulesets/rpim-ruleset.json ${VIRTUAL_ENV}/rpim-ruleset.json

# Default execution
CMD ["scout"]
